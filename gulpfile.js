var gulp = require('gulp');
var browserSync = require('browser-sync');
var reload = browserSync.reload;

gulp.task('serve', function () {
  browserSync({
    notify: false,
    server: {
      baseDir: ['./']
    }
  });

  gulp.watch(['./*.html', './elements/**/*.html'], reload);
  gulp.watch(['./*.css', './elements/**/*.css'], reload);
  gulp.watch(['./*.js', './elements/**/*.js'], reload);
});
